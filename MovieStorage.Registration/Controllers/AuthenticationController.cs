﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using MovieStorage.Registration.Commands;
using MovieStorage.Registration.Filters;
using MovieStorage.Registration.Models;

namespace MovieStorage.Registration.Controllers;

[ApiController]
[Route("[controller]")]
public class AuthenticationController : ControllerBase
{
    private readonly IMediator _mediator;
    
    public AuthenticationController(IMediator mediator)
    {
        _mediator = mediator;
    }
    
    [HttpPost]
    [RegistrationActionFilter]
    [RegistrationErrorFilter]
    [Route("~/register")]
    public async Task<IActionResult> Register([FromBody] UserDTO userDTO)
    {
        var command = new RegisterUserCommand(userDTO);
        var result = await _mediator.Send(command);
        return Ok(result);
    }
}