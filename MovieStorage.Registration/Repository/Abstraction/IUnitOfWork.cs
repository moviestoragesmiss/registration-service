﻿using MovieStorage.Registration.Data.Identity;

namespace MovieStorage.Registration.Repository.Abstraction;

public interface IUnitOfWork : IDisposable
{
    IGenericRepository<ServiceUser> Users{get;}
    
    Task Save();
}