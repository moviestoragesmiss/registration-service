﻿using MediatR;
using MovieStorage.Registration.Models;
using MovieStorage.Registration.Responses;

namespace MovieStorage.Registration.Commands;
public class RegisterUserCommand : IRequest<RegisterUserResponse>
{
    public UserDTO UserDto { get; set; }

    public RegisterUserCommand(UserDTO userDto)
    {
        UserDto = userDto;
    }
}