﻿using AutoMapper;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Identity;
using MovieStorage.Registration.Commands;
using MovieStorage.Registration.Data;
using MovieStorage.Registration.Data.Identity;
using MovieStorage.Registration.Exceptions;
using MovieStorage.Registration.Responses;
using Profile;

namespace MovieStorage.Registration.Handlers;

public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommand, RegisterUserResponse>
{
    private readonly IMapper _mapper;
    private readonly UserManager<ServiceUser> _userManager;
    public readonly IPublishEndpoint _publishEndpoint;

    public RegisterUserCommandHandler(IMapper mapper, UserManager<ServiceUser> userManager, IPublishEndpoint publishEndpoint)
    {
        _mapper = mapper;
        _userManager = userManager;
        _publishEndpoint = publishEndpoint;
    }

    public async Task<RegisterUserResponse> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
    {
        var user = _mapper.Map<ServiceUser>(request.UserDto);
        var result = await _userManager.CreateAsync(user, request.UserDto.Password);
        if (!result.Succeeded)
        {
            throw new RegistrationFailedException(result.Errors);
        } 

        var profile = new ProfileDTO(){UserName = user.UserName};
        await _publishEndpoint.Publish<ProfileDTO>(profile, cancellationToken);
        var response = _mapper.Map<RegisterUserResponse>(user);
        return response;
    }
}