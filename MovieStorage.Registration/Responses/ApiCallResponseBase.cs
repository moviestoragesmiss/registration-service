﻿using System.Net;

namespace MovieStorage.Registration.Responses;

public class ApiCallResponseBase
{
    public string Title;

    public string Description;
    
    public HttpStatusCode StatusCode;

}